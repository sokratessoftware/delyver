# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import os

version = ''.join([x[4:].strip() for x in file(os.path.join(os.path.dirname(__file__), "delyver", "version.inf")).readlines() if x.startswith('ver=')])

setup(
    name = "delyver",
    version = version,
    author = 'SOKRATES-software',
    author_email = "sokrates@sokrates.pl",
    description = "delyver :: SOKRATES-software remote python package installation",
    license = "Proprietary",
    url = "http://www.sokrates.pl",
    zip_safe = True,
    package_data = {"delyver": ["*.inf"]},
    package_dir = {"delyver": "delyver"},
    packages = find_packages (exclude=["tests"]),
    install_requires = ["plumbum"],
    entry_points = {
         "console_scripts": [
            "delyver = delyver:deliver_command",
        ]
    },
    test_suite = "nose.collector",
)
