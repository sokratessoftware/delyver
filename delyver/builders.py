import sys, os, subprocess, glob, tempfile

def setup_py (pkg, temp_path):
    workdir = os.path.abspath (os.getcwd ())
    
    dir = os.path.dirname (pkg.build_from)
    script = os.path.basename (pkg.build_from)
    tempdir = os.path.abspath (tempfile.mkdtemp (dir=temp_path))
    
    try:
        os.chdir (dir)
        
        subprocess.check_call ([sys.executable, script, "sdist", "-d", tempdir, "--formats", "gztar"])
        
        pkg.install_from = pkg.copy_from = glob.glob (os.path.join (tempdir, "*.tar.gz")) [0]
    finally:
        os.chdir (workdir)
