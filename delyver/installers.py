# coding=utf-8
import os, sys, re

# TODO: zrefaktorować powtarzające się rzeczy

def easy_install (machine, pkgs, to, path, temp, upgrade=None, sudo=None, verbose=None):
    flags = []
    
    with machine.session () as session:
        if to == "venv":
            session.run ("cd %s" % path)
            session.run (". bin/activate")
        elif to == "home":
            flags.append ("--user")
        
        if upgrade:
            flags.append ("--upgrade")
        
        args = { "temp":  temp,
                 "flags": " ".join (flags),
                 "pkgs":  " ".join ([pkg.install_from for pkg in pkgs]),
                 "sudo":  "sudo" if sudo else "" }
        
        _, stdout, stderr = session.run ("%(sudo)s easy_install %(flags)s %(pkgs)s" % args, )
        if verbose:
            sys.stdout.write (stdout)
            sys.stderr.write (stderr)

def pip (machine, pkgs, to, path, temp, upgrade=None, sudo=None, verbose=None, insecure=None):
    dldir = os.path.join (temp, "downloads")
    machine["mkdir"] ("-p", dldir)
    flags = []
    
    with machine.session () as session:
        if to == "venv":
            session.run ("cd %s" % path)
            session.run (". bin/activate")
        elif to == "home":
            flags.append ("--user")
        
        if upgrade:
            flags.append ("--upgrade")
        
        if insecure:
            flags.append ("--allow-all-external")
        
        retried = 0
        while 1:
            args = { "temp":  dldir,
                     "flags": " ".join (flags),
                     "pkgs":  " ".join ([pkg.install_from for pkg in pkgs]),
                     "sudo":  "sudo" if sudo else "" }
            
            ret, stdout, stderr = session.run ("%(sudo)s pip install %(flags)s --download=%(temp)s %(pkgs)s" % args, retcode=(0, 1) if not retried else 0)
            
            if not retried:
                any_flags = False
                
                for match in re.finditer (r"Some insecure and unverifiable files were ignored \(use --allow-unverified (.*?) to allow\)\.", stdout):
                    flags.append ("--allow-unverified %s" % match.group (1))
                    any_flags = True
                
                if any_flags:
                    retried += 1
                    continue
            
            break
        
        if verbose:
            sys.stdout.write ("* Downloaded: %s\n" % " ".join (map (str, machine.path (dldir).list ())))
        
        _, stdout, stderr = session.run ("%(sudo)s pip install --no-index --find-links=file://%(temp)s %(flags)s %(pkgs)s" % args, retcode=None)
        if verbose:
            sys.stdout.write (stdout)
            sys.stderr.write (stderr)
