# coding=utf-8
import os, sys, argparse, urlparse, collections, tempfile, shutil

import plumbum, plumbum.path.utils

from . import builders, installers

class Fail (Exception):
    pass

try:
    DELIVER_SSH_LINGER = int (os.environ["DELIVER_SSH_LINGER"])
except Exception:
    DELIVER_SSH_LINGER = 60

class Package (object):
    name = None
    path = None
    build_from = None
    copy_from = None
    install_from = None
    build_fn = None

def deliver_command (args=None):
    if args is None:
        args = sys.argv
    
    parser = argparse.ArgumentParser (prog=args[0], description="Deliver python packages to chosen destinations.", usage="%(prog)s <package> [<package> ...] --to <destination> [--sudo] [--upgrade] [--keep-going]")
    parser.add_argument ("packages", nargs="+", metavar="<package>", help="Names, paths to .egg, .tgz or setup.py")
    parser.add_argument ("--to", metavar="<destination>", help="Destination venv, user directory or / (optionally prefixed by ssh:// URL)")
    parser.add_argument ("-S", "--sudo", action="store_true", help="Use sudo.")
    parser.add_argument ("-U", "--upgrade", action="store_true", help="Upgrade existing packages.")
    parser.add_argument ("-k", "--keepgoing", action="store_true", help="Keep going despite errors.")
    parser.add_argument ("-q", "--quiet", action="store_true", help="Be quiet. Suppress informational output..")
    parser.add_argument ("-v", "--verbose", action="store_true", help="Be verbose. Print subcommand output.")
    parser.add_argument ("-I", "--insecure", action="store_true", help="Allow external, unverified downloads via pip.")
    parser.add_argument ("-K", "--key", metavar="PATH", help="Use specified key to authenticate.")
    
    opts = parser.parse_args (args[1:])
    
    if not opts.to:
        parser.print_help ()
        sys.exit (1)
    
    try:
        deliver (**vars(opts))
    except Fail as ex:
        sys.stderr.write ("%s\n" % ex.message)
        sys.exit (1)
    
def deliver (to, packages, sudo=None, upgrade=None, keepgoing=None, quiet=None, verbose=None, key=None, insecure=None, **kwargs):
    remote = False
    dest = to
    url = urlparse.urlparse (to)
    
    if url.scheme == "ssh":
        remote = True
        if not url.hostname:
            raise Fail ("URL contains not hostname: %s" % to)
        if url.path in ("", "/"):
            raise Fail ("URL contains no path: %s (use // for root, /~ for $HOME)" % to)
        dest = urlparse.urlunparse (("", "") + url[2:])[1:]
    elif url.scheme == "file":
        dest = os.path.join (url.netloc, urlparse.urlunparse (("", "") + url[2:]))
    elif not url.scheme:
        dest = to
    else:
        raise Fail ("Unsupported URL: %s" % to)
    
    def soft_fail (msg):
        if keepgoing:
            sys.stderr.write ("%s\n" % msg)
        else:
            raise Fail (msg)
    
    pkgs = []
    any_builds = False
    egg_installs = []
    pip_installs = []
    
    for spec in packages:
        pkg = Package ()
        name, ext = os.path.splitext (spec.lower ())
        pkg.name = os.path.basename (spec)
        
        try:
            scheme, netloc, path, params, query, fragment = urlparse.urlparse (spec)
        except:
            scheme, netloc, path, params, query, fragment = (None, None, None, None, None, None)
        
        if scheme:
            pkg.install_from = spec
            pip_installs.append (pkg)
            
        elif ext == ".py":
            #pkg.name = os.path.join (os.path.basename (spec)
            any_builds = True
            pkg.build_fn = builders.setup_py
            pkg.build_from = os.path.abspath (spec)
            pip_installs.append (pkg)
            
        elif ext == ".egg":
            pkg.install_from = pkg.copy_from = os.path.abspath (spec)
            egg_installs.append (pkg)
            
        elif ext in (".gz", ".bz2", ".tar", ".zip", ".whl"):
            pkg.install_from = pkg.copy_from = os.path.abspath (spec)
            pip_installs.append (pkg)
            
        else:
            pkg.install_from = spec
            pip_installs.append (pkg)
        
        if pkg.copy_from and not os.path.exists (pkg.copy_from):
            soft_fail ("File doesn't exist: %s\n" % pkg.copy_from)
        else:
            pkgs.append (pkg)
    
    if not pkgs:
        raise Fail ("Nothing to do.")
    
    local_temp_path = None
    remote_temp_path = None
    
    try:
        local_temp_path = os.path.abspath (tempfile.mkdtemp (prefix="deliver_"))
        
        if any_builds:
            if not quiet:
                sys.stdout.write ("=== BUILDING PACKAGES ===\n")
            
            # build phase
            for pkg in pkgs:
                if pkg.build_fn:
                    try:
                        pkg.build_fn (pkg, temp_path=local_temp_path)
                    except Exception as ex:
                        pkg.copy_from = None
                        if verbose:
                            import traceback
                            traceback.print_exc ()
                        soft_fail ("Failed to build %s: %s %s" % (pkg.name, type (ex).__name__, ex.message))
        
        if remote:
            if not quiet:
                sys.stdout.write ("=== ESTABLISHING CONNECTION ===\n")
            
            ssh_opts = ["-o", "ControlMaster=auto",
                        "-o", "ControlPersist=%s" % DELIVER_SSH_LINGER,
                        "-o", "ControlPath=/tmp/deliver_%h_%p_%r.socket"]
            
            try:
                remote = plumbum.SshMachine (url.hostname, user=url.username, port=url.port, keyfile=key,
                                             ssh_opts=ssh_opts, scp_opts=ssh_opts)
            except Exception as ex:
                raise Fail ("Failed to connect: %s %s\n" % (type(ex).__name__, ex.message))
            
            if not quiet:
                res = remote["cat"].run ("/run/motd.dynamic", retcode=(0, 1))
                if res[0] == 0:
                    sys.stdout.write ("%s\n" % res[1])
        
        machine = remote or plumbum.local
        
        if not quiet:
            sys.stdout.write ("=== CHECKING DESTINATION ===\n")
        
        if dest.startswith ("~"):
            dest = remote.env["HOME"] + dest[1:]
        kind, path = scout_path (machine, dest or remote.env["HOME"])
        
        if not quiet:
            if kind == "root":
                sys.stdout.write (" * destination: root filesystem\n")
            
            elif kind == "home":
                sys.stdout.write (" * destination: home at %s/.local/\n" % path)
                if sudo:
                    sys.stdout.write ("   ...refusing to use sudo for that")
                    sudo = False
            
            elif kind == "venv":
                sys.stdout.write (" * destination: virtualenv at %s\n" % path)
                sudo = False
                if sudo:
                    sys.stdout.write ("   ...refusing to use sudo for that")
                    sudo = False
            
            elif kind == "?":
                raise Fail ("Destination doesn't look like /, a home directory or a virtualenv: %s\n" % path)
            
            else:
                raise Fail ("Destination doesn't exist: %s\n" % path)
        
        if remote:
            to_copy = [pkg for pkg in pkgs if pkg.copy_from]
            names = [os.path.basename (pkg.copy_from) for pkg in to_copy]
            nameset = set (names)
            
            if len (names) > len (nameset):
                ctr = collections.Counter (names)
                soft_fail ("Duplicate packages detected: %s" % ", ".join ([name for name, count in ctr.items ()]))
            
            if to_copy:
                if not quiet:
                    sys.stdout.write ("=== UPLOADING FILES ===\n")
                
                remote_temp_path = remote_temp_path or remote["mktemp"] ("-d", "-t", "deliver.XXXXXXXXXX").strip ()
                
                for pkg in to_copy:
                    if not pkg.copy_from: continue
                    
                    base = os.path.basename (pkg.copy_from)
                    if base not in nameset: continue
                    
                    nameset.discard (base)
                    
                    pkg.install_from = os.path.join (remote_temp_path, base)
                    
                    if not quiet:
                        sys.stdout.write (" * %s\n" % pkg.install_from)
                    plumbum.path.utils.copy (plumbum.local.path (pkg.copy_from),
                                             remote.path (pkg.install_from))
        
        to_install = [pkg for pkg in pip_installs if pkg.install_from]
        eggs_to_install = [pkg for pkg in egg_installs if pkg.install_from]
        
        if eggs_to_install:
            if not quiet:
                sys.stdout.write ("=== INSTALLING EGGS ===\n")
            
            installers.easy_install (machine,
                                     pkgs=eggs_to_install,
                                     to=kind,
                                     path=path,
                                     temp=remote_temp_path if remote else local_temp_path,
                                     upgrade=upgrade,
                                     sudo=sudo,
                                     verbose=verbose)
        
        if to_install:
            if not quiet:
                sys.stdout.write ("=== INSTALLING OTHER PACKAGES ===\n")
            
            if remote:
                remote_temp_path = remote_temp_path or remote["mktemp"] ("-d", "-t", "deliver.XXXXXXXXXX").strip ()
            
            installers.pip (machine,
                            pkgs=to_install,
                            to=kind,
                            path=path,
                            temp=remote_temp_path if remote else local_temp_path,
                            upgrade=upgrade,
                            sudo=sudo,
                            insecure=insecure,
                            verbose=verbose)
            
    finally:
        if local_temp_path:
            shutil.rmtree (local_temp_path, ignore_errors=True)
        
        if remote_temp_path and remote:
            remote["rm"] ("-rf", remote_temp_path)

def scout_path (context, path):
    remote_path = context.path (path)
    if not remote_path.exists ():
        return (None, path)
    
    path = context["readlink"] ("-f", path).strip ()
    if path == "/":
        return ("root", "/")
    
    activate = os.path.join (path, "bin", "activate")
    pip = os.path.join (path, "bin", "pip")
    if context.path (activate).exists () and context.path (pip).exists ():
        return ("venv", path)
    
    home = context["readlink"] ("-f", context.env["HOME"]).strip ()
    if path.rstrip ("/") == home.rstrip ("/"):
        return ("home", path)
    
    return ("?", path)
