delyver
=======

`delyver` to narzędzie do zdalnej instalacji pakietów pythonowych w różnych
formach.

Wywołanie
---------

    delyver pakiet pakiet pakiet --to przeznaczenie


Obsługiwane formy pakietów
--------------------------

  * *katalogi źródłowe* - należy podać ścieżkę do setup.py, pakiet zostanie
                          spakowany lokalnie i zbudowany zdalnie
  
  * *pakiety źródłowe* - pakiety .tar.gz, .tar.bz2, .zip tworzone przez sdist
  
  * *pakiety .whl*
  
  * *eggi* - mogą być problematyczne
  
  * *pobrania z PYPI* - nazwa, która nie kończy się charakterystycznym
                        rozszerzeniem pliku jest przekazywana bez zmian do pipa

Obsługiwane przeznaczenia
-------------------------

### Lokalne katalogi ###

  * system - należy podać ścieżkę / i zapewne wypada dodać argument --sudo
  * katalog domowy
  * virtualenv

### Ścieżki SSH ###

    ssh://user@host:port/ścieżka
    
    np. ssh://user@host:port// - root
        ssh://user@host:port/~ - katalog domowy
        ssh://user@host:port/foo - katalog foo relatywnie do katalogu domowego

Formy `ssh://user@host:port` i `ssh://user@host:port/` są niedozwolone w celu
uniknięcia potencjalnych nieporozumień.
